// Copyright (C) 2002-2005 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine" and the "irrXML" project.
// For conditions of distribution and use, see copyright notice in irrlicht.h and/or irrXML.h

#ifndef __ICXML_READER_IMPL_H_INCLUDED__
#define __ICXML_READER_IMPL_H_INCLUDED__

#include "IrrXml.hpp"
#include "irrString.h"
#include "irrArray.h"
#include "fast_atof.h"

#include "Exception/AttributeNotExist.hpp"

#include <string>
#include <vector>
#include <map>

namespace irr::io
{

//! implementation of the IrrXMLReader
	class CXmlReaderImpl : public IIrrXMLReader
	{

	private:

		// structure for storing attribute-name pairs
		class Attribute
		{

		public:

			std::string Name;
			std::string Value;
		};

	private:

		// Fields

		char* TextData = nullptr;         // data block of the text file
		char* currentPoint = nullptr;     // current point in text to parse
		char* TextBegin = nullptr;        // start of text to parse

		unsigned int TextSize = 0;       // size of text to parse in characters, not bytes

		XmlNode CurrentNodeType;   // type of the currently parsed node

		std::string NodeName;    // name of the node currently in

		bool IsEmptyElement{ };       // is the currently parsed node empty?

		std::vector <Attribute> Attributes; // attributes of current element

	public:

		//! Constructor
		explicit CXmlReaderImpl(IFileReadCallBack* callback, bool deleteCallBack = true)
		{
			if (callback == nullptr)
			{
				return;
			}

			CurrentNodeType = XmlNode::None;

			// read whole xml file

			readFile(callback);

			// clean up

			if (deleteCallBack)
			{
				delete callback;
			}

			// set pointer to text begin
			currentPoint = TextBegin;
		}


		//! Destructor
		~CXmlReaderImpl() override
		{
			delete[] TextData;
		}


		//! Reads forward to the next xml node.
		//! \return Returns false, if there was no further node.
		bool read() override
		{
			// if not end reached, parse the node
			if (currentPoint && (unsigned int)(currentPoint - TextBegin) < TextSize - 1 && *currentPoint != 0)
			{
				parseCurrentNode();
				return true;
			}

			return false;
		}


		//! Returns the type of the current XML node.
		[[nodiscard]] XmlNode getNodeType() const override
		{
			return CurrentNodeType;
		}


		//! Returns attribute count of the current XML node.
		[[nodiscard]] int getAttributeCount() const override
		{
			return Attributes.size();
		}


		//! Returns name of an attribute.
		[[nodiscard]] const std::string& getAttributeName(int idx) const override
		{
			if (idx < 0 || idx >= Attributes.size())
			{
				throw ExceptionAttributeNotExist("Not Exist Attribute.");
			}

			return Attributes[idx].Name;
		}


		//! Returns the value of an attribute.
		[[nodiscard]] const std::string& getAttributeValue(int idx) const override
		{
			if (idx < 0 || idx >= Attributes.size())
			{
				throw ExceptionAttributeNotExist("Not Exist Attribute.");
			}

			return Attributes[idx].Value;
		}


		//! Returns the value of an attribute.
		[[nodiscard]] const std::string& getAttributeValue(const std::string& name) const override
		{
			const Attribute* attr = getAttributeByName(name);

			return attr->Value;
		}


		//! Returns the value of an attribute as integer.
		[[nodiscard]] int getAttributeValueAsInt(const std::string& name) const override
		{
			const Attribute* attr = getAttributeByName(name);

			return std::stoi(attr->Value);
		}


		//! Returns the value of an attribute as integer.
		[[nodiscard]] int getAttributeValueAsInt(int idx) const override
		{
			return (int)getAttributeValueAsFloat(idx);
		}


		//! Returns the value of an attribute as float.
		[[nodiscard]] float getAttributeValueAsFloat(const std::string& name) const override
		{
			const Attribute* attr = getAttributeByName(name);

			return std::stof(attr->Value);
		}


		//! Returns the value of an attribute as float.
		[[nodiscard]] float getAttributeValueAsFloat(int idx) const override
		{
			const std::string attrvalue = getAttributeValue(idx);

			return std::stof(attrvalue);
		}


		//! Returns the name of the current node.
		[[nodiscard]] const std::string& getNodeName() const override
		{
			return NodeName;
		}


		//! Returns data of the current node.
		[[nodiscard]] const std::string& getNodeData() const override
		{
			return NodeName;
		}


		//! Returns if an element is an empty element, like <foo />
		[[nodiscard]] bool isEmptyElement() const override
		{
			return IsEmptyElement;
		}

	private:

		// Reads the current xml node
		void parseCurrentNode()
		{
			char* start = currentPoint;

			// more forward until '<' found
			while (*currentPoint != L'<' && *currentPoint)
			{
				++currentPoint;
			}

			if (!*currentPoint)
			{
				return;
			}

			if (currentPoint - start > 0)
			{
				// we found some text, store it
				if (setText(start, currentPoint))
				{
					return;
				}
			}

			++currentPoint;

			// based on current token, parse and report next element
			switch (*currentPoint)
			{
			case L'/':
				parseClosingXMLElement();
				break;
			case L'?':
				ignoreDefinition();
				break;
			case L'!':
				if (!parseCDATA())
				{
					parseComment();
				}
				break;
			default:
				parseOpeningXMLElement();
				break;
			}
		}


		//! sets the state that text was found. Returns true if set should be set
		bool setText(char* start, char* end)
		{
			// check if text is more than 2 characters, and if not, check if there is
			// only white space, so that this text won't be reported
			if (end - start < 3)
			{
				char* p = start;
				for (; p != end; ++p)
				{
					if (!isWhiteSpace(*p))
					{
						break;
					}
				}

				if (p == end)
				{
					return false;
				}
			}

			// set current text to the parsed text, and replace xml special characters
			core::string <char> s(start, (int)(end - start));

			NodeName = s.c_str();
			replaceSpecialCharacters(NodeName);

			// current XML node type is text
			CurrentNodeType = XmlNode::Text;

			return true;
		}


		//! ignores an xml definition like <?xml something />
		void ignoreDefinition()
		{
			CurrentNodeType = XmlNode::Unknown;

			// move until end marked with '>' reached
			while (*currentPoint != L'>')
			{
				++currentPoint;
			}

			++currentPoint;
		}


		//! parses a comment
		void parseComment()
		{
			CurrentNodeType = XmlNode::Comment;
			currentPoint += 1;

			std::string pCommentBegin(currentPoint);
			int positionEndTag = pCommentBegin.find('>');
			positionEndTag -= 4;

			int count = 1;

			// move until end of comment reached
			while (count)
			{
				if (*currentPoint == L'>')
				{
					--count;
				}
				else if (*currentPoint == L'<')
				{
					++count;
				}

				++currentPoint;
			}

			currentPoint -= 3;
			NodeName = pCommentBegin.substr(2, positionEndTag);
			currentPoint += 3;
		}


		//! parses an opening xml element and reads attributes
		void parseOpeningXMLElement()
		{
			CurrentNodeType = XmlNode::Element;
			IsEmptyElement = false;
			Attributes.clear();

			// find name
			std::string startName(currentPoint);

			// find end of element
			while (*currentPoint != L'>' && !isWhiteSpace(*currentPoint))
			{
				++currentPoint;
			}

			// find Attributes
			while (*currentPoint != L'>')
			{
				if (isWhiteSpace(*currentPoint))
				{
					++currentPoint;
				}
				else
				{
					if (*currentPoint != L'/')
					{
						// we've got an attribute

						// read the attribute names
						std::string attributeNameBegin(currentPoint);

						while (!isWhiteSpace(*currentPoint) && *currentPoint != L'=')
						{
							++currentPoint;
						}

						int attributeNameEnd = attributeNameBegin.find('=');
						++currentPoint;

						// read the attribute value
						// check for quotes and single quotes, thx to murphy
						while ((*currentPoint != L'\"') && (*currentPoint != L'\'') && *currentPoint)
						{
							++currentPoint;
						}

						if (!*currentPoint)
						{ // malformatted xml file
							return;
						}

						const char attributeQuoteChar = *currentPoint;

						++currentPoint;
						std::string attributeValueBegin(currentPoint);

						while (*currentPoint != attributeQuoteChar && *currentPoint)
						{
							++currentPoint;
						}

						if (!*currentPoint)
						{ // malformatted xml file
							return;
						}

						int attributeValueEnd = attributeValueBegin.find('\"');
						++currentPoint;

						Attribute attr;
						attr.Name = attributeNameBegin.substr(0, attributeNameEnd);

						attr.Value = attributeValueBegin.substr(0, attributeValueEnd);

						replaceSpecialCharacters(attr.Value);
						Attributes.push_back(attr);
					}
					else
					{
						// tag is closed directly
						++currentPoint;
						IsEmptyElement = true;
						break;
					}
				}
			}

			// Find a ' ' or a '>'
			int positionEndTag = startName.find_first_of(" >");


			if (positionEndTag != 0)
			{
				// check if this tag is closing directly
				if (startName.at(positionEndTag - 1) == '/')
				{
					// directly closing tag
					IsEmptyElement = true;
				}
			}

			NodeName = startName.substr(0, positionEndTag);

			++currentPoint;
		}


		//! parses an closing xml tag
		void parseClosingXMLElement()
		{
			CurrentNodeType = XmlNode::ElementEnd;
			IsEmptyElement = false;
			Attributes.clear();

			++currentPoint;
			std::string pBeginClose(currentPoint);
			int positionEndTag = pBeginClose.find('>');

			while (*currentPoint != L'>')
			{
				++currentPoint;
			}

			NodeName = pBeginClose.substr(0, positionEndTag);
			++currentPoint;
		}

		//! parses a possible CDATA section, returns false if begin was not a CDATA section
		bool parseCDATA()
		{
			if (*(currentPoint + 1) != L'[')
			{
				return false;
			}

			CurrentNodeType = XmlNode::CData;

			// skip '<![CDATA['
			int count = 0;
			while (*currentPoint && count < 8)
			{
				++currentPoint;
				++count;
			}

			if (!*currentPoint)
			{
				return true;
			}

			std::string cDataBegin(currentPoint);
			int cDataEnd = cDataBegin.find("]]>");
			cDataEnd -= 2;

			if (cDataEnd)
			{
				NodeName = cDataBegin.substr(0, cDataEnd);
			}
			else
			{
				NodeName = "";
			}

			return true;
		}

		// finds a current attribute by name, returns 0 if not found
		[[nodiscard]] const Attribute* getAttributeByName(const std::string& name) const
		{
			for (const Attribute& Attribute : Attributes)
			{
				if (Attribute.Name == name)
				{
					return &Attribute;
				}
			}

			return nullptr;
		}

		static void replaceSpecialCharacters(std::string& origin)
		{
			std::map <char, std::string> specialCharacters;

			// Special Character : <
			specialCharacters['<'] = "lt;";
			// Special Character : >
			specialCharacters['>'] = "gt;";
			// Special Character : &
			specialCharacters['&'] = "amp;";
			// Special Character : "
			specialCharacters['\"'] = "quot;";
			// Special Character : '
			specialCharacters['\''] = "apos;";

			// All special character init with a &
			// Example: &amp; --> is replaced as: &
			// Example: &lt; --> is replaced as: <
			// Example: &quot; --> is replaced as: "
			int positionAmpersand = origin.find('&');
			int positionSemmicolon = origin.find(';');

			if (positionAmpersand == std::string::npos)
			{
				return;
			}

			while (positionAmpersand != std::string::npos)
			{
				int lengthSubString = positionSemmicolon - positionAmpersand;

				std::string subString = origin.substr(
						positionAmpersand + 1, lengthSubString);

				for (auto& x : specialCharacters)
				{
					if (subString == x.second)
					{
						int lenght = positionSemmicolon - positionAmpersand;
						origin.replace(positionAmpersand, lenght + 1, 1, x.first);

						break;
					}
				}

				positionAmpersand = origin.find('&');
				positionSemmicolon = origin.find(';');

				if (positionAmpersand == std::string::npos ||
					positionSemmicolon == std::string::npos)
				{
					break;
				}
			}
		}

		//! reads the xml file and converts it into the wanted character format.
		bool readFile(IFileReadCallBack* callback)
		{
			int size = callback->getSize();
			size += 4; // We need two terminating 0's at the end.
			// For ASCII we need 1 0's, for UTF-16 2, for UTF-32 4.

			char* data8 = new char[size];

			if (!callback->read(data8, size - 4))
			{
				delete[] data8;
				return false;
			}

			// add zeros at end

			data8[size - 1] = 0;
			data8[size - 2] = 0;
			data8[size - 3] = 0;
			data8[size - 4] = 0;

			// UTF-8
			TextBegin = data8 + 3;
			TextData = data8;
			TextSize = size;

			return true;
		}


		//! returns true if a character is whitespace
		static inline bool isWhiteSpace(char c)
		{
			return (c == ' ' || c == '\t' || c == '\n' || c == '\r');
		}

	}; // end CXMLReaderImpl


} // end namespace

#endif
