// Copyright (C) 2002-2005 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine" and the "irrXML" project.
// For conditions of distribution and use, see copyright notice in irrlicht.h and/or irrXML.h

#ifndef __IRR_XML_H_INCLUDED__
#define __IRR_XML_H_INCLUDED__

#include <cstdio>
#include <bits/stringfwd.h>

namespace irr::io
{
	//! Enumeration for all xml nodes which are parsed by IrrXMLReader
	enum class XmlNode : short
	{
		//! No xml node. This is usually the node if you did not read anything yet.
				None,

		//! A xml element, like <foo>
				Element,

		//! End of an xml element, like </foo>
				ElementEnd,

		//! Text within a xml element: <foo> this is the text. </foo>
				Text,

		//! An xml comment like &lt;!-- I am a comment --&gt; or a DTD definition.
				Comment,

		//! An xml cdata section like &lt;![CDATA[ this is some CDATA ]]&gt;
				CData,

		//! Unknown element.
				Unknown
	};

	//! Callback class for file read abstraction.
	/** With this, it is possible to make the xml parser read in other things
	than just files. The Irrlicht engine is using this for example to
	read xml from compressed .zip files. To make the parser read in
	any other data, derive a class from this interface, implement the
	two methods to read your data and give a pointer to an instance of
	your implementation when calling createIrrXMLReader(),
	createIrrXMLReaderUTF16() or createIrrXMLReaderUTF32() */
	class IFileReadCallBack
	{
	public:

		//! virtual destructor
		virtual ~IFileReadCallBack() = default;

		/**
		 * Reads an amount of bytes from the file.
		 *
		 * @param buffer Pointer to buffer where to read bytes will be written to.
		 * @param sizeToRead Amount of bytes to read from the file.
		 * @return Returns how much bytes were read.
		 */
		virtual bool read(char* buffer, int sizeToRead) = 0;

		/**
		 * @return Returns size of file in bytes
		 */
		virtual int getSize() = 0;
	};

	//! Interface providing easy read access to a XML file.
	/** You can create an instance of this reader using one of the factory functions
	createIrrXMLReader(), createIrrXMLReaderUTF16() and createIrrXMLReaderUTF32().
	If using the parser from the Irrlicht Engine, please use IFileSystem::createXMLReader()
	instead.
	For a detailed intro how to use the parser, see \ref irrxmlexample and \ref features.

	The typical usage of this parser looks like this:
	\code
	#include <irrXML.h>
	using namespace irr; // irrXML is located in the namespace irr::io
	using namespace io;

	void main()
	{
		// create the reader using one of the factory functions
		IrrXMLReader* xml = createIrrXMLReader("Config.xml");

		if (xml == 0)
			return; // file could not be opened

		// parse the file until end reached
		while(xml->read())
		{
			// based on xml->getNodeType(), do something.
		}

		// delete the xml parser after usage
		delete xml;
	}
	\endcode
	See \ref irrxmlexample for a more detailed example.
	*/
	class IIrrXMLReader
	{

	public:

		//! Destructor
		virtual ~IIrrXMLReader() = default;;

		//! Reads forward to the next xml node.
		/** \return Returns false, if there was no further node.  */
		virtual bool read() = 0;

		//! Returns the type of the current XML node.
		[[nodiscard]] virtual XmlNode getNodeType() const = 0;

		//! Returns attribute count of the current XML node.
		/** This is usually
		non null if the current node is EXN_ELEMENT, and the element has attributes.
		\return Returns amount of attributes of this xml node. */
		[[nodiscard]] virtual int getAttributeCount() const = 0;

		//! Returns name of an attribute.
		/** \param idx: Zero based index, should be something between 0 and getAttributeCount()-1.
		\return Name of the attribute, 0 if an attribute with this index does not exist. */
		[[nodiscard]] virtual const std::string& getAttributeName(int idx) const = 0;

		//! Returns the value of an attribute.
		/** \param idx: Zero based index, should be something between 0 and getAttributeCount()-1.
		\return Value of the attribute, 0 if an attribute with this index does not exist. */
		[[nodiscard]] virtual const std::string& getAttributeValue(int idx) const = 0;

		//! Returns the value of an attribute.
		/** \param name: Name of the attribute.
		\return Value of the attribute, 0 if an attribute with this name does not exist. */
		[[nodiscard]] virtual const std::string& getAttributeValue(const std::string& name) const = 0;

		//! Returns the value of an attribute in a safe way.

		//! Returns the value of an attribute as integer.
		/** \param name Name of the attribute.
		\return Value of the attribute as integer, and 0 if an attribute with this name does not exist or
		the value could not be interpreted as integer. */
		[[nodiscard]] virtual int getAttributeValueAsInt(const std::string& name) const = 0;

		//! Returns the value of an attribute as integer.
		/** \param idx: Zero based index, should be something between 0 and getAttributeCount()-1.
		\return Value of the attribute as integer, and 0 if an attribute with this index does not exist or
		the value could not be interpreted as integer. */
		[[nodiscard]] virtual int getAttributeValueAsInt(int idx) const = 0;

		//! Returns the value of an attribute as float.
		/** \param name: Name of the attribute.
		\return Value of the attribute as float, and 0 if an attribute with this name does not exist or
		the value could not be interpreted as float. */
		[[nodiscard]] virtual float getAttributeValueAsFloat(const std::string& name) const = 0;

		//! Returns the value of an attribute as float.
		/** \param idx: Zero based index, should be something between 0 and getAttributeCount()-1.
		\return Value of the attribute as float, and 0 if an attribute with this index does not exist or
		the value could not be interpreted as float. */
		[[nodiscard]] virtual float getAttributeValueAsFloat(int idx) const = 0;

		//! Returns the name of the current node.
		/** Only non null, if the node type is EXN_ELEMENT.
		\return Name of the current node or 0 if the node has no name. */
		[[nodiscard]] virtual const std::string& getNodeName() const = 0;

		//! Returns data of the current node.
		/** Only non null if the node has some
		data and it is of type EXN_TEXT or EXN_UNKNOWN. */
		[[nodiscard]] virtual const std::string& getNodeData() const = 0;

		//! Returns if an element is an empty element, like <foo />
		[[nodiscard]] virtual bool isEmptyElement() const = 0;
	};

	//! A UTF-8 or ASCII character xml parser.
	/** This means that all character data will be returned in 8 bit ASCII or UTF-8 by this parser.
	The file to read can be in any format, it will be converted to UTF-8 if it is not
	in this format.
	Create an instance of this with createIrrXMLReader();
	See IIrrXMLReader for description on how to use it. */
	typedef IIrrXMLReader IrrXMLReader;


	//! Creates an instance of an UFT-8 or ASCII character xml parser.

	/** This means that all character data will be returned in 8 bit ASCII or UTF-8.
	The file to read can be in any format, it will be converted to UTF-8 if it is not in this format.
	If you are using the Irrlicht Engine, it is better not to use this function but
	IFileSystem::createXMLReaderUTF8() instead.
	\param filename: Name of file to be opened.
	\return Returns a pointer to the created xml parser. This pointer should be
	deleted using 'delete' after no longer needed. Returns 0 if an error occured
	and the file could not be opened. */
	IrrXMLReader* createIrrXMLReader(const std::string& filename);
}

#endif // __IRR_XML_H_INCLUDED__

