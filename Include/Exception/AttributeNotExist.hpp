#ifndef IRRXML_EXCEPTIONATTRIBUTENOTEXIST_HPP
#define IRRXML_EXCEPTIONATTRIBUTENOTEXIST_HPP

#include <exception>
#include <string>
#include <iostream>

namespace irr
{

	class ExceptionAttributeNotExist : public std::exception
	{

	private:

		std::string messageError;

	public:

		explicit ExceptionAttributeNotExist(const std::string& error)
		{
			messageError = error;
		}

		void printMessage()
		{
			std::cout << messageError << "\n";
		}
	};
}

#endif //IRRXML_EXCEPTIONATTRIBUTENOTEXIST_HPP
