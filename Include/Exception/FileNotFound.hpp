#ifndef IRRXML_FILENOTFOUND_HPP
#define IRRXML_FILENOTFOUND_HPP

#include <exception>
#include <string>
#include <iostream>

namespace irr
{

	class ExceptionFileNotFound : public std::exception
	{

	private:

		std::string messageError;

	public:

		explicit ExceptionFileNotFound(const std::string& error)
		{
			messageError = error;
		}

		void printMessage()
		{
			std::cout << messageError << "\n";
		}
	};
}

#endif //IRRXML_FILENOTFOUND_HPP
