# IrrXML

Welcome to irrXML

IrrXML is intended to be a high speed and easy-to-use XML Parser for C++

### Directory structure overview

  You will find some directories after decompressing the archive in which
  came the SDK. These are:
  
  \doc         Documentation of irrXML.
  \example     A short example showing how to use the parser with solution
               and make files.
  \src         The source code of irrXML.   

### How to use

  For Linux/Unix users: Simply go into the directory /example and run
  'make'. This will create a sample project using irrXML.
  
  Windows users: Just add the source files to your project. That's all. 
  For more information see the documentation in the \doc directory.
    
  Alternatively, you could compile irrXML as .lib, after this you would
  only need to use the irrXML.h header file, nothing more.

### Contact

  If you have problems, questions or suggestions, please visit the 
  official homepage of irrXML:
  
  https://www.ambiera.com/irrxml/
  
  You will find forums, patches, documentation, and other stuff
  which will help you out.
  
  If want to contact the author of the engine, please send an email to
  Nikolaus Gebhardt:
  
  irrlicht@users.sourceforge.net


  
    