#include "IrrXml.hpp"

using namespace irr;
using namespace io;

#include <string>
#include <iostream>

#include "Exception/FileNotFound.hpp"

int main()
{
	IrrXMLReader* xml;

	try
	{
		xml = createIrrXMLReader("Config.xml");
	}
	catch (ExceptionFileNotFound& e)
	{
		e.printMessage();
		return 0;
	}

	// strings for storing the data we want to get out of the file
	std::string modelFile;
	std::string messageText;
	std::string caption;
	std::string cdata;
	float valueOfPI;

	// parse the file until end reached
	while (xml && xml->read())
	{
		switch (xml->getNodeType())
		{
		case XmlNode::Text:
			// in this xml file, the only text which occurs is the messageText
			messageText = xml->getNodeData();
			break;
		case XmlNode::CData:
			cdata = xml->getNodeData();
			break;
		case XmlNode::Element:
		{
			if (xml->getNodeName() == "startUpModel")
			{
				modelFile = xml->getAttributeValue("file");
			}
			else if (xml->getNodeName() == "messageText")
			{
				caption = xml->getAttributeValue("caption");
			}
			else if (xml->getNodeName() == "floatvalue")
			{
				valueOfPI = xml->getAttributeValueAsFloat("pi");
			}
		}
			break;
		}
	}

	std::cout << modelFile << "\n\n";
	std::cout << messageText << "\n\n";
	std::cout << caption << "\n\n";
	std::cout << valueOfPI << "\n\n";
	std::cout << cdata << "\n\n";

	// delete the xml parser after usage
	delete xml;
	return 0;
}
