// Copyright (C) 2002-2005 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine" and the "irrXML" project.
// For conditions of distribution and use, see copyright notice in irrlicht.h and/or irrXML.h

#include "IrrXml.hpp"
#include "CXmlReaderImpl.h"
#include "Exception/FileNotFound.hpp"

#include <fstream>

namespace irr::io
{

	/**
	 * Implementation of the file read callback for ordinary files
	 */
	class CFileReadCallBack : public IFileReadCallBack
	{

	private:

		// Fields

		std::ifstream file;
		int size = 0;

	public:

		//! construct from filename
		explicit CFileReadCallBack(const std::string& filename)
		{
			// open file in mode of only read
			file.open(filename, std::ios::in);

			if (file.is_open())
			{
				// Retrieves the file size of the open file
				file.seekg(0, std::ifstream::end);
				size = file.tellg();
				file.seekg(0, std::ifstream::beg);
			}
			else
			{
				throw ExceptionFileNotFound("Cannot open the file: " + filename);
			}
		}

		//! destructor
		~CFileReadCallBack() override
		{
			file.close();
		}

		//! Reads an amount of bytes from the file.
		bool read(char* buffer, int sizeToRead) override
		{
			file.read(buffer, sizeToRead);

			return true;
		}

		//! Returns size of file in bytes
		int getSize() override
		{
			return size;
		}

	};



// FACTORY FUNCTIONS:


//! Creates an instance of an UFT-8 or ASCII character xml parser. 
	IrrXMLReader* createIrrXMLReader(const std::string& filename)
	{
		return new CXmlReaderImpl(new CFileReadCallBack(filename));
	}


//! Creates an instance of an UFT-8 or ASCII character xml parser.
	IrrXMLReader* createIrrXMLReader(IFileReadCallBack* callback)
	{
		return new CXmlReaderImpl(callback, false);
	}


} // end namespace irr
